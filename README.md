# Ansible OpenNebula Role

This role adds the [OpenNebular Comminuty Edition repos for
Debian/Ubuntu](https://docs.opennebula.io/6.2/installation_and_configuration/frontend_installation/opennebula_repository_configuration.html#id1)
and then installs the packages for a [Single Front-end Installation of
OpenNebula](https://docs.opennebula.io/6.2/installation_and_configuration/frontend_installation/install.html#single-front-end-installation),
see the [defaults/main.yml](defaults/main.yml) file for the version and
list of packages.

This role uses tasks from the [MariaDB role](https://git.coop/webarch/mariadb)
to read the `oneadmin` database password from `/var/lib/one/.my.cnf`, the login
details are written there by the [users role](https://git.coop/webarch/users),
see the [repo to build a testing
server](https://git.coop/webarch/opennebula-server) for an example of this.
